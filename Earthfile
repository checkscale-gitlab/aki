FROM registry.gitlab.com/ayedocloudsolutions/adf:4722ddfcc8362bb65eba6294f9a7d587acaef434

ARG DOCKER_VERSION=20.10.3
ARG MKDOCS_VERSION=6.1.7
ARG NGINX_VERSION=1.21
ARG HELM_VERSION=3.6.0
ARG KUBECTL_VERSION=1.21.0
ARG PORTER_VERSION=v0.33.0
ARG TERRAFORM_VERSION=0.14.11
ARG SUPER_LINTER_VERSION=v3.14.3
ARG GITLEAKS_VERSION=v6.1.2

ARG IN_CI=0
ARG EARTHLY_GIT_HASH # a2h5d6gcf
ARG GIT_COMMIT_HASH=${EARTHLY_GIT_HASH}
ARG EARTHLY_GIT_ORIGIN_URL # https://gitlab.com/ayedocloudsolutions/adf
ARG GIT_ORIGIN_URL=${EARTHLY_GIT_ORIGIN_URL}
ARG GIT_AUTHOR_EMAIL=info@ayedo.de
ARG GIT_AUTHOR_NAME=ayedo Bot
ARG GIT_COMMITTER_EMAIL=info@ayedo.de
ARG GIT_COMMITTER_NAME=ayedo Bot
ARG APP_AUTHOR # ayedocloudsolutions
ARG APP_NAME # adf
ARG EARTHLY_GIT_PROJECT_NAME # ayedocloudsolutions/adf
ARG APP_PROJECT_NAME=${EARTHLY_GIT_PROJECT_NAME}
ARG APP_BUILD_DATE # 2021-07-10 13:55
ARG APP_BUILD_VERSION=${GIT_COMMIT_HASH} # a2h5d6gcf
ARG APP_CONTAINER_REGISTRY=registry.gitlab.com # registry.gitlab.com
ARG APP_CONTAINER_IMAGE=${APP_PROJECT_NAME}:${APP_BUILD_VERSION} # ayedocloudsolutions/adf:a2h5d6gcf
ARG APP_CONTAINER_IMAGE_URL=${APP_CONTAINER_REGISTRY}/${APP_CONTAINER_IMAGE} # registry.gitlab.com/ayedocloudsolutions/adf:a2h5d6gcf

ARG GITLAB_TOKEN
ARG GL_TOKEN=${GITLAB_TOKEN}
ARG GITHUB_TOKEN
ARG GH_TOKEN=${GITHUB_TOKEN}

RUN apk add --no-cache bash

utils.project-data:
  COPY . .

utils.docker-cli:
  FROM docker:${DOCKER_VERSION}
  SAVE ARTIFACT /usr/local/bin/docker

utils.env:
  RUN env

utils.ssh:
  RUN echo 'github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==' >> known_hosts && \
      echo 'gitlab.com ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCsj2bNKTBSpIYDEGk9KxsGh3mySTRgMtXL583qmBpzeQ+jqCMRgBqB98u3z++J1sKlXHWfM9dyhSevkMwSbhoR8XIq/U0tCNyokEi/ueaBMCvbcTHhO7FcwzY92WK4Yt0aGROY5qX2UKSeOvuP4D6TPqKF1onrSzH9bx9XUf2lEdWT/ia1NEKjunUqu1xOB/StKDHMoX4/OKyIzuS0q/T1zOATthvasJFoPrAjkohTyaDUz2LN5JoH839hViyEG82yB+MjcFV5MU3N1l1QL3cVUCh93xSaua1N85qivl+siMkPGbO5xR/En4iEY6K2XPASUEMaieWVNTRCtJ4S8H+9' >> known_hosts
  SAVE ARTIFACT known_hosts

utils.build-version:
    FROM +utils.project-data
    # fix/ci_refactor -> ci-refactor
    RUN export BUILD_BRANCH=$(git rev-parse --abbrev-ref HEAD | tr _/ -)
    RUN export BUILD_COMMIT=$(git rev-parse --short HEAD)
    # v0.2.0-alpha -> 0.2.0
    #_LATEST_VERSION=$(git describe --abbrev=0 --tags | cut -d'-' -f1 || echo "0.0.1")
    RUN export _LATEST_VERSION=$(git describe --abbrev=0 --tags | cut -d'-' -f1 || echo "0.0.1")
    #RUN export SEMVER_REGEX="^v?([0-9]+\.[0-9]+\.[0-9]+)(?:-([0-9A-Za-z-]+(?:\.[0-9A-Za-z-]+)*))?(?:\+[0-9A-Za-z-]+)?$"
    RUN if [[ ${_LATEST_VERSION} =~ ^v ]]; then export LATEST_VERSION=${_LATEST_VERSION:1}; else export LATEST_VERSION=${_LATEST_VERSION}; fi
    # Fix: if no git tag exists, set to 0.0.1
    # if [ -z "$LATEST_VERSION" ];
    # then
    #   echo "No git tag found"
    #   gitTag=v0.0.1
    # fi
    RUN export BUILD_VERSION="$LATEST_VERSION-$BUILD_BRANCH.$BUILD_COMMIT"
    #BUILD_VERSION="${_VERSION:1}"
    RUN echo ""
    RUN echo "Full git hash: $CI_COMMIT_SHA"
    RUN echo "Short git hash: $BUILD_COMMIT"
    RUN echo "Latest git tag: $LATEST_VERSION"
    RUN echo "Current git branch: $BUILD_BRANCH"
    RUN echo "Compiled build version: $BUILD_VERSION"
    RUN echo ""
    RUN echo $BUILD_VERSION > BUILD_VERSION
    RUN echo $LATEST_VERSION > LATEST_VERSION

prepare:


build:
  BUILD +build.docs
  BUILD +build.docker

# Build
build.go:
  FROM golang:1.16-alpine
  WORKDIR /app
  COPY go.mod .
  COPY go.sum .
  RUN go mod download
  COPY . .
  RUN go install .
  ENTRYPOINT ["acs"]
  CMD ["--version"]
  SAVE IMAGE --push registry.gitlab.com/ayedocloudsolutions/acs:latest

build.docs:
  FROM squidfunk/mkdocs-material:$MKDOCS_VERSION
  RUN mkdir -p /app/public &&\
      pip3 install --no-cache-dir mkdocs-awesome-pages-plugin==2.4.0 \
      mkdocs-git-revision-date-localized-plugin==0.7.2 \
      mkdocs-macros-plugin==0.5.0
  WORKDIR /app
  COPY mkdocs.yml ./
  COPY docs docs
  RUN mkdocs build --clean
  SAVE ARTIFACT public AS LOCAL public

build.docker:
  BUILD --platform=linux/amd64 \
        #--platform=linux/arm/v7 \
        --platform=linux/arm64 \
  +build.docker.base

build.docker.base:
  FROM DOCKERFILE .

  LABEL org.label-schema.schema-version="1.0"
  LABEL org.label-schema.build-date=$APP_BUILD_DATE
  LABEL org.label-schema.name=$APP_CONTAINER_IMAGE
  LABEL org.label-schema.description=$APP_NAME
  LABEL org.label-schema.url=$GIT_ORIGIN_URL
  LABEL org.label-schema.vcs-url=$GIT_ORIGIN_URL
  LABEL org.label-schema.vcs-ref=$GIT_COMMIT_HASH
  LABEL org.label-schema.vcs-type="Git"
  LABEL org.label-schema.vendor=$APP_AUTHOR
  LABEL org.label-schema.version=$APP_BUILD_VERSION
  LABEL org.label-schema.docker.dockerfile="/Dockerfile"

  RUN echo "Saving image ${APP_CONTAINER_IMAGE_URL}"

  SAVE IMAGE --push ${APP_CONTAINER_IMAGE_URL}

# Test
test:
  BUILD +test.unit
  BUILD +test.integration
  BUILD +test.qa

test.unit:
  RUN echo "Doing unit tests"

test.integration:
  RUN echo "Doing integration tests"

test.docker:
  FROM earthly/dind:alpine
    WITH DOCKER --load app:latest=+build.docker.base
        RUN docker run app:latest
    END
  # FROM +publish.docker.base
  # RUN for command in docker helm kubectl ansible-playbook terraform skaffold; do \
  #       which $command || (echo "$command not installed" && exit 1); \
  #     done

test.qa:
  BUILD +test.qa.lint
  BUILD +test.qa.secrets

test.qa.lint:
  FROM github/super-linter:$SUPER_LINTER_VERSION
  COPY . .
  ENV RUN_LOCAL="true"
  ENV DEFAULT_WORKSPACE=.
  ENV DEFAULT_BRANCH=main
  ENV LINTER_RULES_PATH=".linters"
  ENV OUTPUT_FORMAT="tap"
  ENV OUTPUT_DETAILS="detailed"
  ENV OUTPUT_FOLDER="super-linter.report"
  ENV CONVERTED_OUTPUT_FOLDER="converted-xml.report"
  ENV TAP_JUNIT_VERSION="4.0.0"
  ENV REPORT_SUITE_TEST_NAME="super_linter"
  ENV FILTER_REGEX_EXCLUDE="CHANGELOG.md"
  RUN /action/lib/linter.sh
  SAVE ARTIFACT ${OUTPUT_FOLDER}/*.tap

# QA
test.qa.lint.gitlab:  
  WORKDIR $OUTPUT_FOLDER
  RUN npm install -g tap-junit@${TAP_JUNIT_VERSION} && \
      mkdir -p ${CONVERTED_OUTPUT_FOLDER} /tmp && \
      for report in *; do \
      'sed -i "s/message: \*\+/message: /g" $report' && \
      cat $report | tap-junit -p -s "${REPORT_SUITE_TEST_NAME}" > ../${CONVERTED_OUTPUT_FOLDER}/${report}.xml && \
      sed -i 's/<failure message="\(.\+\)" type="fail">.*/<failure message="" type="fail">\n\1\n<\/failure>/g' ../${CONVERTED_OUTPUT_FOLDER}/${report}.xml && \
      sed -i 's/\\n/\n/g' ../${CONVERTED_OUTPUT_FOLDER}/${report}.xml && \
      sed -i ':begin;$!N;s/<\/failure>\n<\/failure>/<\/failure>/;tbegin;P;D' ../${CONVERTED_OUTPUT_FOLDER}/${report}.xml; \
      done
  SAVE ARTIFACT ${OUTPUT_FOLDER}/*.tap

test.qa.secrets:
  FROM zricethezav/gitleaks:$GITLEAKS_VERSION
  COPY . .
  RUN gitleaks -v --pretty --repo-path . --report gitleaks-report.json
  SAVE ARTIFACT gitleaks-report.json

# Release
release.code:
  FROM node:14
  COPY +utils.ssh/known_hosts /root/.ssh/known_hosts
  COPY . .
  RUN touch CHANGELOG.md && \
      npm install @semantic-release/exec @semantic-release/git @semantic-release/gitlab @semantic-release/changelog -D && \
      npx semantic-release --no-ci
  SAVE ARTIFACT CHANGELOG.md AS LOCAL CHANGELOG.md
  SAVE ARTIFACT RELEASE_VERSION AS LOCAL RELEASE_VERSION

# Publish
publish.docker:
  BUILD --build-arg APP_BUILD_VERSION publish.docker.version

publish.docker.version:
  FROM +build.docker
  ARG RELEASE_VERSION
  RUN echo "Saving image ${APP_CONTAINER_REGISTRY}/${APP_PROJECT_NAME}:${RELEASE_VERSION}"
  SAVE IMAGE --push ${APP_CONTAINER_REGISTRY}/${APP_PROJECT_NAME}:${RELEASE_VERSION}

pr:
  BUILD +qa
  BUILD +build
  BUILD +test
  BUILD +publish.docker.base