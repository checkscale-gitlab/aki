variable "hcloud_token" {}
variable "stack" {}

variable "ssh_public_key" {
    default = "~/.ssh/id_rsa.pub"
}

variable "master_count" {
    default = 1
}

variable "master_type" {
    default = "cx31"
}

variable "master_location" {
    default = "fsn1"
}

variable "node_count" {
    default = 3
}

variable "node_type" {
    default = "cx31"
}

variable "node_location" {
    default = "fsn1"
}

variable "os_image" {
    default = "ubuntu-20.04"
}
